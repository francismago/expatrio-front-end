const profile = JSON.parse(localStorage.getItem('profile'))
const accessToken = JSON.parse(localStorage.getItem('accessToken'))

const initialState = {
  profile: profile || null,
  accessToken: accessToken || null
}

function setProfile(state, action) {
  localStorage.setItem('profile', JSON.stringify(action.payload.profileObject))

  return {
    ...state,
    profile: action.payload.profileObject
  }
}

function setAccessToken(state, action) {
  localStorage.setItem('accessToken', JSON.stringify(action.payload.accessToken))

  return {
    ...state,
    accessToken: action.payload.accessToken
  }
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case 'AUTH/SET_PROFILE':
      return setProfile(state, action)
    case 'AUTH/SET_ACCESS_TOKEN':
      return setAccessToken(state, action)
    default:
      break
  }
  return state
}

export default reducer