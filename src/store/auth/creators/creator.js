export function setProfile(profileObject) {
  return {
    type: 'AUTH/SET_PROFILE',
    payload: {
      profileObject,
    },
  }
}

export function setAccessToken(accessToken) {
  return {
    type: 'AUTH/SET_ACCESS_TOKEN',
    payload: {
      accessToken,
    },
  }
}