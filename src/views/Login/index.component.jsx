import React from 'react'
import {Link as RouterLink} from 'react-router-dom'
import {
    Container,
    Typography,
    Link,
    makeStyles,
    Avatar,
    TextField,
    FormControlLabel,
    Checkbox,
    Button,
    Grid
} from '@material-ui/core'
import {LockOutlined as LockOutlinedIcon} from '@material-ui/icons'
import {Redirect} from 'react-router-dom'

import client from '../../services/client'

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))

const Login = (props) => {
    const {history, setProfile, setAccessToken, accessToken} = props

    const classes = useStyles()

    const handleLoginSubmit = e => {
        e.preventDefault()
        const {email, password} = e.currentTarget
        const formData = new FormData()
        formData.append('username', email.value)
        formData.append('password', password.value)
        formData.append('grant_type', 'password')

        client.post('/login', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                auth: {
                    username: process.env.REACT_APP_KEY_ID,
                    password: process.env.REACT_APP_KEY_SECRET
                }
            },
        )
            .then(res => {
                console.log('fullName',res.data.full_name)
              console.log('accessToken',res.data.access_token)
                setProfile(res.data.full_name)
                setAccessToken(res.data.access_token)

                history.push('/')
            })
    }

    if (accessToken) {
        return <Redirect to="/"/>
    }

    return (
        <Container component='main' maxWidth='xs'>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component='h1' variant='h5'>
                    Log in
                </Typography>
                <form className={classes.form} noValidate onSubmit={handleLoginSubmit}>
                    <TextField
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        id='email'
                        label='Email Address'
                        name='email'
                        autoComplete='email'
                        autoFocus
                    />
                    <TextField
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        name='password'
                        label='Password'
                        type='password'
                        id='password'
                        autoComplete='current-password'
                    />
                    <Button
                        type='submit'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                    >
                        Log In
                    </Button>
                </form>
            </div>
        </Container>
    )
}

export default Login