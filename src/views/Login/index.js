import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Login from './index.component'
import { setAccessToken, setProfile } from '../../store/auth/creators/creator'

const mapStateToProps = state => {
  const { auth } = state
  const { accessToken } = auth

  return {
    accessToken
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setAccessToken,
    setProfile
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)