import React from 'react'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { People as PeopleIcon, ExitToApp as ExitToAppIcon, } from '@material-ui/icons'

export const ListItems = (props) => (
  <div>
    <ListItem button>
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary='Customers' />
    </ListItem>
    <ListItem button onClick={props.onLogout}>
      <ListItemIcon>
        <ExitToAppIcon />
      </ListItemIcon>
      <ListItemText primary='Logout' />
    </ListItem>
  </div>
)
