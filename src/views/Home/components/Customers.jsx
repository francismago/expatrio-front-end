import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import {lighten, makeStyles} from '@material-ui/core/styles'
import {
    TableCell,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
    Toolbar,
    Typography,
    IconButton,
    Tooltip,
    FormControlLabel,
    Switch,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    ButtonBase,
    Button,
    TextField,
    MenuItem,
    InputLabel,
    Select
} from '@material-ui/core'
import {Delete as DeleteIcon, Add as AddIcon, Edit as EditIcon} from '@material-ui/icons'

import client from '../../../services/client'

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1
    }
    if (b[orderBy] > a[orderBy]) {
        return 1
    }
    return 0
}

function stableSort(array, cmp) {
    if (!array) return
    const stabilizedThis = array.map((el, index) => [el, index])
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0])
        if (order !== 0) return order
        return a[1] - b[1]
    })
    return stabilizedThis.map(el => el[0])
}

function getSorting(order, orderBy) {
    return order === 'desc'
        ? (a, b) => desc(a, b, orderBy)
        : (a, b) => -desc(a, b, orderBy)
}

const headCells = [
    {id: 'firstName', numeric: false, disablePadding: false, label: 'First Name'},
    {id: 'middleName', numeric: false, disablePadding: false, label: 'Middle Name'},
    {id: 'lastName', numeric: false, disablePadding: false, label: 'Last Name'},
    {id: 'gender', numeric: false, disablePadding: false, label: 'Gender'},
    {id: 'birthDate', numeric: false, disablePadding: false, label: 'Birth Date'},
    {id: 'email', numeric: false, disablePadding: false, label: 'Email'},
    {id: 'mobileNumber', numeric: false, disablePadding: false, label: 'Mobile Number'},
    {id: 'address', numeric: false, disablePadding: false, label: 'Address'},
]

function EnhancedTableHead(props) {
    const {
        classes,
        order,
        orderBy,
        onRequestSort,
    } = props
    const createSortHandler = property => event => {
        onRequestSort(event, property)
    }

    return (
        <TableHead>
            <TableRow>
                <TableCell padding='checkbox'>
                </TableCell>
                {headCells.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    )
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
}

const useToolbarStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
}))

const EnhancedTableToolbar = props => {
    const classes = useToolbarStyles()
    const {numSelected, setAddCustomer} = props

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <Typography className={classes.title} variant='h6' id='tableTitle'>
                Customers
            </Typography>

            <Tooltip title='Add Customer'>
                <IconButton aria-label='add' onClick={() => setAddCustomer(true)}>
                    <AddIcon/>
                </IconButton>
            </Tooltip>
        </Toolbar>
    )
}

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
}

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}))

function Customers(props) {
    const {rows = [], accessToken} = props
    const classes = useStyles()
    const [order, setOrder] = React.useState('asc')
    const [orderBy, setOrderBy] = React.useState('calories')
    const [selected, setSelected] = React.useState([])
    const [page, setPage] = React.useState(0)
    const [dense, setDense] = React.useState(false)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [addCustomer, setAddCustomer] = React.useState(false)
    const [editRow, setEditRow] = React.useState(null)
    const [deleteRow, setDeleteRow] = React.useState(null)
    const [gender, setGender] = React.useState('')

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc'
        setOrder(isAsc ? 'desc' : 'asc')
        setOrderBy(property)
    }

    const handleSelectAllClick = event => {
        if (!rows.length) return

        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.name)
            setSelected(newSelecteds)
            return
        }
        setSelected([])
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    const handleChangeDense = event => {
        setDense(event.target.checked)
    }

    const isSelected = name => selected.indexOf(name) !== -1

    const emptyRows =
        rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage)

    const handleDeleteAgree = () => {
        client.delete(`/customers/${deleteRow.id}`,
            {
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            })
            .then(res => {
                rows.splice(rows.indexOf(deleteRow), 1)

                setDeleteRow(null)
            })
    }

    const handleAddEditFormSubmit = e => {
        e.preventDefault()

        const {firstName, middleName, lastName, birthDate, email, mobileNumber, address} = e.currentTarget

        if (addCustomer) {
            client.post('/customers', {
                    firstName: firstName.value,
                    middleName: middleName.value,
                    lastName: lastName.value,
                    gender: gender,
                    birthDate: birthDate.value,
                    email: email.value,
                    contactNumber: mobileNumber.value,
                    address: address.value
                }, {
                    headers: {
                        'Authorization': `Bearer ${accessToken}`
                    }
                }
            )
                .then(res => {
                    console.log('try try')
                    console.log('res.data', res.data)
                    rows.unshift(res.data)
                    console.log('rows', rows)


                    setAddCustomer(false)
                })
        }

        if (editRow) {
            client.put(`/customers/${editRow.id}`, {
                firstName: firstName.value,
                middleName: middleName.value,
                lastName: lastName.value,
                gender: gender,
                birthDate: birthDate.value,
                email: email.value,
                contactNumber: mobileNumber.value,
                address: address.value
            }, {
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            })
                .then(res => {
                    rows.splice(rows.indexOf(editRow), 1, res.data)

                    setEditRow(null)
                })
        }
    }

    const handleSetGender = g => {
        setGender(g.target.value)
    }

    return (
        <div className={classes.root}>
            <EnhancedTableToolbar
                numSelected={selected.length}
                setAddCustomer={setAddCustomer}
            />
            <TableContainer>
                <Table
                    className={classes.table}
                    aria-labelledby='tableTitle'
                    size={dense ? 'small' : 'medium'}
                    aria-label='enhanced table'
                >
                    <EnhancedTableHead
                        classes={classes}
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                    />
                    <TableBody>
                        {stableSort(rows, getSorting(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(row => {
                                const isItemSelected = isSelected(row.name)

                                return (
                                    <TableRow
                                        hover
                                        role='checkbox'
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={row.id}
                                        selected={isItemSelected}
                                    >
                                        <TableCell padding='none'>
                                            <div style={{display: 'flex'}}>
                                                <Tooltip title={`Edit`}>
                                                    <IconButton
                                                        aria-label='edit'
                                                        onClick={() => setEditRow(row)}
                                                    >
                                                        <EditIcon/>
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip title={`Delete`}>
                                                    <IconButton
                                                        aria-label='delete'
                                                        onClick={() => setDeleteRow(row)}
                                                    >
                                                        <DeleteIcon/>
                                                    </IconButton>
                                                </Tooltip>
                                            </div>
                                        </TableCell>
                                        <TableCell>{row.firstName}</TableCell>
                                        <TableCell>{row.middleName}</TableCell>
                                        <TableCell>{row.lastName}</TableCell>
                                        <TableCell>{row.gender === 'M' ? 'Male' : 'Female'}</TableCell>
                                        <TableCell>{row.birthDate}</TableCell>
                                        <TableCell>{row.email}</TableCell>
                                        <TableCell>{row.contactNumber}</TableCell>
                                        <TableCell>{row.address}</TableCell>
                                        <TableCell>{row.mobileNumber}</TableCell>
                                    </TableRow>
                                )
                            })}
                        {emptyRows > 0 && (
                            <TableRow style={{height: (dense ? 33 : 53) * emptyRows}}>
                                <TableCell colSpan={6}/>
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component='div'
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
            <FormControlLabel
                control={<Switch checked={dense} onChange={handleChangeDense}/>}
                label='Dense padding'
            />
            <Dialog
                open={!!deleteRow}
                aria-labelledby='alert-dialog-title'
                aria-describedby='alert-dialog-description'
            >
                <DialogTitle id='alert-dialog-title'>
                    {`Delete ${!!deleteRow ? deleteRow.name : ''}`}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id='alert-dialog-description'>
                        Are you sure you want to delete this customer?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setDeleteRow(null)} color='primary'>
                        No
                    </Button>
                    <Button onClick={handleDeleteAgree} color='primary' autoFocus>
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={addCustomer || editRow}
                aria-labelledby='form-dialog-title'
                fullWidth
                maxWidth='sm'
            >
                <DialogTitle id='form-dialog-title'>
                    {`${addCustomer ? 'Add Customer' : editRow && 'Edit Customer'}`}
                </DialogTitle>
                <form onSubmit={handleAddEditFormSubmit}>
                    <DialogContent>
                        <DialogContentText>
                            {addCustomer && 'Please enter customer details'}
                            {editRow && 'Edit customer details'}
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin='normal'
                            id='firstName'
                            name='firstName'
                            label='First Name'
                            type='text'
                            defaultValue={editRow && editRow.firstName}
                            fullWidth
                        />
                        <TextField
                            margin='normal'
                            id='middleName'
                            name='middleName'
                            label='Middle Name'
                            type='text'
                            defaultValue={editRow && editRow.middleName}
                            fullWidth
                        />
                        <TextField
                            margin='normal'
                            id='lastName'
                            name='lastName'
                            label='Last Name'
                            type='text'
                            defaultValue={editRow && editRow.lastName}
                            fullWidth
                        />
                        <InputLabel
                            id='gender-label'>Gender</InputLabel>
                        <Select
                            margin='normal'
                            labelId='gender-label'
                            id='gender'
                            onChange={handleSetGender}
                            value={gender}
                            defaultValue={editRow && editRow.gender}
                            fullWidth>
                            <MenuItem value='M'>Male</MenuItem>
                            <MenuItem value='F'>Female</MenuItem>
                        </Select>
                        <TextField
                            margin='normal'
                            id='birthDate'
                            name='birthDate'
                            label='Birth Date'
                            type='date'
                            defaultValue={editRow && editRow.birthDate}
                            fullWidth
                        />
                        <TextField
                            margin='normal'
                            id='email'
                            name='email'
                            label='Email Address'
                            type='email'
                            defaultValue={editRow && editRow.email}
                            fullWidth
                        />
                        <TextField
                            margin='normal'
                            id='mobileNumber'
                            name='mobileNumber'
                            label='Mobile Number'
                            type='text'
                            defaultValue={editRow && editRow.contactNumber}
                            fullWidth
                        />
                        <TextField
                            margin='normal'
                            id='address'
                            name='address'
                            label='Address'
                            type='text'
                            defaultValue={editRow && editRow.address}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={() => {
                                setAddCustomer(false)
                                setEditRow(null)
                            }}
                            color='primary'
                        >
                            Cancel
                        </Button>
                        <Button color='primary' type="submit">
                            Submit
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    )
}

export default Customers
