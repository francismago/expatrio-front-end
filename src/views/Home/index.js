import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Home from './index.component'
import { setProfile, setAccessToken } from '../../store/auth/creators/creator'

const mapStateToProps = state => {
  const { auth } = state
  const { profile, accessToken } = auth

  return {
    profile,
    accessToken
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setProfile,
    setAccessToken
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)