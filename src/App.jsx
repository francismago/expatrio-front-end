import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Login from './views/Login/'
import Home from './views/Home/'

function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/login" component={Login} />
    </Router>
  );
}

export default App;
